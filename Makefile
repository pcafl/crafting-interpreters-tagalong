CLASS_PREFIX=com/craftinginterpreters/lox
CLASSES=$(CLASS_PREFIX)/Lox.class $(CLASS_PREFIX)/TokenType.class $(CLASS_PREFIX)/Token.class $(CLASS_PREFIX)/Scanner.class

TOOLS_PREFIX=com/craftinginterpreters/tool
TOOLS_CLASSES=$(TOOLS_PREFIX)/GenerateAst.java

JAVAC=javac
JAVA=java

.PHONY: all program clean tools

all: lox.jar tools

lox.jar: $(CLASSES)
	jar -e com.craftinginterpreters.lox.Lox -c -v -f $@ $^

tools: generate_ast.jar

generate_ast.jar: com/craftinginterpreters/tool/GenerateAst.class
	jar -e com.craftinginterpreters.tool.GenerateAst -c -v -f $@  $^

run_repl: lox.jar
	java -jar lox.jar

%.class: %.java
	$(JAVAC) $<

# $(TOOLS_PREFIX)/%.class: $(TOOLS_PREFIX)/%.java
# 	$(JAVAC) $<


# $(CLASS_PREFIX)/%.class: $(CLASS_PREFIX)/%.java
# 	$(JAVAC) $<

clean:
	rm -f $(CLASSES) "com/craftinginterpreters/tool/GenerateAst.class" generate_ast.jar
